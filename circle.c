#include<stdio.h>
#include<math.h>
float input_radius()
{
  float radius; /*local to input*/
  printf("enter radius\n");
  scanf("%f",&radius);
  return radius;
}

float compute_area(float radius)
{
  return M_PI*radius*radius;
}
void output(float radius,float area)
{
  printf("the area of the circle with radius =%f is %f\n",radius,area);
}

float compute_circumference(float radius)
{
  return 2*M_PI*radius;
}
void output_circumference(float radius ,float circumference)
{
  printf("the circumference of the circle with radius %f is %f\n",radius,circumference);
}
int main()
{
  float r,area,circumference;
  r=input_radius();
  area=compute_area(r);
  circumference=compute_circumference(r);
  output(r,area);
  output_circumference(r,circumference);
  return 0;
}